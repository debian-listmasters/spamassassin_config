# -*- mode: spamassassin -*-

## This file contains rules which match various automatic responders
## which give us backscatter. The scores here are best guesses; and
## may need to be overridden for recipients which actually want these
## bounces.

# don 2009-02-05
body  QMAILBOUNCE	/This\s*is\s*the\s*qmail-send\s*program/i
describe QMAILBOUNCE	Stupid qmail bounces; we don't want them
score QMAILBOUNCE	2

# don 2009-02-05 (the following are all for various stupid auto-repsonse things)
header RECEIVEDMAIL	subject =~ /(?:received\s*your\s*mail|(?:respuesta|risposta|response) (?:autom.tica|automatic)|auto(?:matic|m.tica)?\s*(?:respuesta|risposta|response))/i
describe RECEIVEDMAIL	It's great that you've received our mail and automatically responded. We don't care
score RECEIVEDMAIL	4

body AUTOREBOD		/(?:received\s*your\s*mail|(?:(?:respuesta|risposta|response|message) auto(?:m.tica?|mated)?)|auto(?:m.tica?|mated)?\s*(?:respuesta|risposta|response|message))/i
describe AUTOREBOD	It's great that you've automatically responded
score AUTOREBOD		4

header YOURMESSAGE	subject =~ /your message/i
describe YOURMESSAGE	It's great that our message did something; we don't care
score YOURMESSAGE	2.5

body YOURMESSAGEBOD	/^Your message/i
describe YOURMESSAGEBOD	It's great that our message did something; we don't care
score YOURMESSAGEBOD	2.5

body NODELIVERY		/(?:could not be|has not been) delivered/i
describe NODELIVERY	We don't care if it could be or has not been delivered
score NODELIVERY	1.5

body NORESEND		/do not resend your original message/i
describe NORESEND	If you don't want us to resend our message, why tell us?
score NORESEND		2

header PROOFPOINT	from =~ /proofpoint/
describe PROOFPOINT	Automatic mail from proofpoint (some MTA thingie)
score PROOFPOINT	3.5

body  NOTPROCBOUNCE	/was not processed by our system/i
describe NOTPROCBOUNCE	Bounce by system that was not processed
score NOTPROCBOUNCE	2

body ACCOUNTNOTEXIST	/(?:account\s+\S+\s+(does\s*not|doesn't)\s*exist|(?:invalid|not a valid) e-?mail address)/i
describe ACCOUNTNOTEXIST	It's not our problem if an account doesn't exist
score ACCOUNTNOTEXIST		2

body CR_SYSTEM1		/sent by a human and not a computer/i
describe CR_SYSTEM1	Looks like a challenge/response system
score CR_SYSTEM1	2

body CR_SYSTEM2		/do not reply/i
describe CR_SYSTEM2	Body contains do not reply; likely a CR system
score CR_SYSTEM2	1.5

body CR_SYSTEM3		/confirm this request/i
describe CR_SYSTEM3	Body contains confirm this request; likely a CR system
score CR_SYSTEM3	1.5

header CR_SYSTEM4	subject =~ /challenge.*response/i
describe CR_SYSTEM4	Subject contains challenge/response
score CR_SYSTEM4	3

body CR_SYSTEM5		/confirmation of list posting/i
describe CR_SYSTEM5	Body asks us to confirm a list posting
score CR_SYSTEM5	3

header CR_SYSTEM6	subject =~ /^confirm\:/i
describe CR_SYSTEM6	Subject asks us to confirm something; we don't want to
score CR_SYSTEM6	2

header CR_SYSTEM7	x-cr-puzzleid =~ /./i
describe CR_SYSTEM7	Has a X-cr-puzzleid: header
score CR_SYSTEM7	5

body CR_SYSTEM8		/reply to this message/i
describe CR_SYSTEM8	Reply to this message? are you crazy?
score CR_SYSTEM8	3

body SUPPORTMAIL1	/assigned a ticket/
describe SUPPORTMAIL1	Message from an automated support/response system
score SUPPORTMAIL1	2

header SUPPORTMAIL2	subject =~ /^\[*(?:update\s*to\s*)?ticket/
describe SUPPORTMAIL2	Message with ticket leading it; probably a support mail
score SUPPORTMAIL2	1.5

body SUPPORTMAIL3	/the email address \S+ is not registered/i
describe SUPPORTMAIL3	We don't care if an e-mail address is not registered
score SUPPORTMAIL3	1.5

body SUPPORTMAIL4	/(reached an unmonitored e-mail address|no response will be given)/i
describe SUPPORTMAIL4	Yeay for dumb auto-response bots that don't want a response
score SUPPORTMAIL4	1.5

header SUPPORTMAIL5	from =~ /\b(?:support|info)\@/i
describe SUPPORTMAIL5	Message from an address that looks like support@ or info@
score SUPPORTMAIL5	2

header FROMAUTOREPLY 	from =~ /(autoreply|no-?repl(?:y|ies))/i
describe FROMAUTOREPLY	Message from an autoreplier or something who doesn't seem to want a reply
score FROMAUTOREPLY	1

header FROMABUSE	from =~ /\babbuse\@/i
describe FROMABUSE	Message from abbuse@somedomain
score FROMABUSE		1.5

header FROMSPAMCOP	from =~ /spamcop\@/i
describe FROMSPAMCOP	Message from spamcop@somedomain
score FROMSPAMCOP	3.5

header SUBJECTSPAM	subject =~ /(?:\*\*|\[)\s*spam\s*(?:\*\*|\])/i
describe SUBJECTSPAM	Subject contains **spam** or [spam], so it's probably spam
score SUBJECTSPAM	3.5

header MESBLOCKED	subject =~ /your message (?:is )?blocked/i
describe MESBLOCKED	That's great that our message was blocked
score	 MESBLOCKED	2

header FAILDELIVERY	subject =~ /failed to deliver/i
describe FAILDELIVERY	Oh noes! You failed to deliver spam!
score FAILDELIVERY	2

header NEWADDRESS	subject =~ /(?:new e-?mail address|deaktivierung der mailadresse)/i
describe NEWADDRESS	We don't care that you have a new e-mail address
score NEWADDRESS	4

header VACATIONMAIL1	user-agent =~ /vacation/i
describe VACATIONMAIL1	Mail from the vacation user agent
score VACATIONMAIL1	0.1

header VACATIONMAIL2	X-Vacation =~ /./
describe VACATIONMAIL2	Has an X-Vacation header
score VACATIONMAIL2	0.1

meta  VACATIONMAIL	(VACATIONMAIL1 || VACATIONMAIL2)
describe VACATIONMAIL	Looks like a vacation message
score VACATIONMAIL	1