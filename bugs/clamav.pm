# This provides a spamassassin clamav plugin and
# is released under the terms of the GPL version 3, or any later
# version (at your option). See the file README and COPYING for more
# information.
# Copyright 2017 by Don Armstrong <don@donarmstrong.com>.

package clamav;

=head1 NAME

clamav -- Clamav scanner

=head1 SYNOPSIS

Plugin for spamssassin which checks mail using clamav

=head1 DESCRIPTION


=head1 BUGS

None known.

=cut


use warnings;
use strict;

use ClamAV::Client;

use Mail::SpamAssassin;
use base qw(Mail::SpamAssassin::Plugin);

sub new {
    my ($class,@opts) = @_;
    $class = ref($class) // $class;
    my $self = $class->SUPER::new(@opts);
    bless($self,$class);
    $self->register_eval_rule("check_clamav");
    return $self;
}

sub check_clamav {
    my ($self,$pms,$fulltext) = @_;

    my $scanner = ClamAV::Client->new();
    if (not defined $scanner) {
        dbg("ClamAv is not running or could not connect to it");
        return 0;
    }
    my $ret = 0;
    my $infected = $scanner->scan_scalar(\$fulltext);
    if (defined $infected) {
        $ret = 1;
        $pms->set_tag('CHECKCLAMAV','Yes '.$infected);
        $pms->get_message->put_metadata('X-Spam-Virus','Yes '.$infected);
    } else {
        $pms->set_tag('CHECKCLAMAV','No');
        $pms->get_message->put_metadata('X-Spam-Virus','No');
    }
    return $ret;
}



1;


__END__
