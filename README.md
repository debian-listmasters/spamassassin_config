# Listmaster/Debbugs Spamassassin Configuration #

the bugs/ directory contains the debbugs user_prefs file

the list/ directory contains the l.d.o user_prefs file (and later will
contain amavis policy file(s))

put rules into common/ and symlink common into .spamassassin from
wherever you checkout this setup.

# Automatic Updates #

This repository automatically updates itself if the tip commit is
signed by a key in the keyring `keyring.gpg`.

Currently, the keyring `./keyring.gpg` can be updated by running
`./update_keyring`.


